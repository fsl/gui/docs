# FSL GUI Documentation

All FSL graphical user interfaces (GUIs) are organised in separate Gitlab repos and published as conda packages on the FSL conda channel. 

The list of all GUI repos can be found [here](https://git.fmrib.ox.ac.uk/fsl/gui). 

GUIs are published to the FSL conda [development channel](https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/development/) (while under rapid development)

# Development 

FSL GUI development is divided into two parts:
- the [electron](https://www.electronjs.org) app, which provides the overall app framework and interactivity with the operating system and FSL tools
- the GUIs, which are developed as [React](https://react.dev) web apps and loaded in electron browser windows

## Electron app

The electron app is named `fsl-desktop` and is developed in the [`fsl/gui/desktop`](https://git.fmrib.ox.ac.uk/fsl/gui/desktop) repo.

## GUIs

GUIs are developed as React web apps, and are loaded in electron browser windows.

It is expected that GUIs will be developed in separate repos, and then published as conda packages to the FSL conda channel.

**Important**: All GUI conda packages must list the `fsl-gui-desktop` package as a dependency. This ensures that the GUI will be able to load in the electron app. See [fsl/conda/fsl-gui-bet](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-gui-bet) for an example of how to configure this when publishing a GUI as a conda package.

The [`fsl/gui/gui-template`](https://git.fmrib.ox.ac.uk/fsl/gui/gui-template) repo provides a template for developing GUIs.

## Building Conda Packages locally

see [here](https://git.fmrib.ox.ac.uk/fsl/conda/docs/-/blob/master/building_fsl_conda_packages.md?ref_type=heads)

## Creating new conda recipe repos

More details [here](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-ci-rules#bootstrapping-a-new-project). Be sure to create a Gitlab token with API level access first. 

1. install `fsl-ci-rules`
    - `fslpython -m pip install git+https://git.fmrib.ox.ac.uk/fsl/conda/fsl-ci-rules.git`

2. `create_conda_recipe -t $FSLCITOKEN -pp fsl/gui/<GUI_NAME> -rp fsl/conda/fsl-gui-<GUI_NAME> -pt unknown ./fsl-gui-<GUI_NAME>`

3. configure the conda build files. See [fsl/conda/fsl-gui-bet](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-gui-bet) for an example.

4. `configure_repositories -t $FSLCITOKEN fsl/gui/<GUI_NAME> fsl/conda/fsl-gui-<GUI_NAME>`